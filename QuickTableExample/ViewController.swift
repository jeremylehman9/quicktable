//
//  ViewController.swift
//  QuickTableExample
//
//  Created by Ben Weitzman on 8/1/19.
//

import UIKit
import QuickTable
import Foundation


class ViewController: UIViewController {

  let tableView: UITableView = UITableView(frame: .zero, style: .grouped)

  lazy var data: QuickTable<TestSection> = self.dataSource()

  init(viewModel: ViewModel) {
    super.init(nibName: .none, bundle: .none)

    self.view.addSubview(tableView)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
    tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true

    data.setTableView(self.tableView)
    data.items = viewModel.data
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  func dataSource() -> QuickTable<TestSection> {
    return QuickTable { model in
      switch model {
      case .about:
        return Binder(AboutCell.self) {
          $0.textLabel?.text = "About"
        }
      case let .user(name):
        return Binder(UserCell.self) {
          $0.textLabel?.text = name
        }.onSelect { indexPath in
          self.tableView.deselectRow(at: indexPath, animated: false)
          print("tapped \(name)")
        }
      case .footer:
        return Binder(FooterCell.self) {
          $0.textLabel?.text = "Footer"
        }
      }
    }.titleForSection { section in
      switch section {
      case let .users(letter: letter, items: _): return letter
      default: return .none
      }
    }
  }
}

class AboutCell: UITableViewCell {}

class UserCell: UITableViewCell {
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.contentView.backgroundColor = UIColor.lightGray
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

class FooterCell: UITableViewCell {}

