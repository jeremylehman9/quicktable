public protocol QuickTableSection {
  associatedtype Item

  var items: [Item] { get }
}

public class QuickTable<A: QuickTableSection>: NSObject,  UITableViewDataSource, UITableViewDelegate {
  public var items: [A] = [] {
    didSet {
      tableView?.reloadData()
    }
  }

  private weak var tableView: UITableView?

  private let config: (A.Item) -> Binder
  private var getTitle: ((A) -> String?)?

  public init(_ config: @escaping (A.Item) -> Binder) {
    self.config = config
  }

  public func setTableView(_ table: UITableView) {
    table.dataSource = self
    table.delegate = self
    self.tableView = table
  }

  public func numberOfSections(in tableView: UITableView) -> Int {
    return items.count
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items[section].items.count
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = self.items[indexPath.section].items[indexPath.row]
    let binder = config(item)
    let cell = binder.getCell(tableView, indexPath)
    binder.bind(cell)
    return cell
  }

  public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    let item = self.items[section]
    return self.getTitle?(item)
  }

  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = self.items[indexPath.section].items[indexPath.row]
    let binder = config(item)
    binder.handleSelect?(indexPath)
  }

  public func titleForSection(_ getTitle: @escaping (A) -> String?) -> Self {
    self.getTitle = getTitle
    return self
  }
}

public class Binder {
  let clazz: Any.Type
  let bind: (UITableViewCell) -> Void
  let getCell: (UITableView, IndexPath) -> UITableViewCell

  var handleSelect: ((IndexPath) -> Void)?

  public init<T: UITableViewCell>(_ clazz: T.Type, bind: @escaping (T) -> Void) {
    self.clazz = clazz
    self.bind = { cell in
      guard let cell = cell as? T else { return }

      bind(cell)
    }

    self.getCell = { (table, indexPath) in
      let cell: T = table.dequeueReusableCell(forIndexPath: indexPath)
      return cell as UITableViewCell
    }
  }

  public func onSelect(_ handleSelect: @escaping (IndexPath) -> Void) -> Binder {
    self.handleSelect = handleSelect
    return self
  }
}

internal extension UITableViewCell {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

internal extension UITableView {
  func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
    self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
  }
}

/*

 QuickTableDataSource { (model) in
   switch model {
   case .option1 -> register(Option1.self) {
   }
   }
 }
 */
