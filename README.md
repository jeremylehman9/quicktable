[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

QuickTable is a lightweight library for declaring table views backed by lists.

Instead of using the standard table view delegate/data source functions, you can
use QuickTable to specify table configuration and behavior in one place:

```swift
QuickTable { model in
  switch model {
  case .about:
    return Binder(AboutCell.self) {
      $0.textLabel?.text = "About"
    }
  case let .user(name):
    return Binder(UserCell.self) {
      $0.textLabel?.text = name
    }.onSelect { indexPath in
      self.tableView.deselectRow(at: indexPath, animated: false)
      print("tapped \(name)")
    }
  case .footer:
    return Binder(FooterCell.self) {
      $0.textLabel?.text = "Footer"
    }
  }
}.titleForSection { section in
  switch section {
  case let .users(letter: letter, items: _): return letter
  default: return .none
  }
}
```


# Installation

Add to your Cartfile: 
`git "https://www.gitlab.com/benweitzman/quicktable" "master"`